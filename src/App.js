import React from "react";
import "./App.css";
import { Provider } from "react-redux";

import Select from "./components/GameControler";
import Field from "./components/Field";
import LeaderBoard from './components/LeaderBoard';
import store from "./store";

function App() {
  return (
    <Provider store={store}>
      <div className="container ">
        <h1 className="text-center">Wellcome!</h1>
        <div className='d-flex justify-content-between flex-wrap'>
          <div className='d-flex align-items-center flex-column'>
            <Select />
            <Field />
          </div>
          <LeaderBoard/>
        </div>
      </div>
    </Provider>
  );
}

export default App;
