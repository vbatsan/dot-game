import{SETSETTINGS, SETNAME, STATUS_NEW, STATUS_OLD, START_GAME, END_GAME} from './types';

export const setGameSettings = payload => dispatch => (
    dispatch({type: SETSETTINGS, payload})
)

export const setPlayerName = payload => dispatch => (
    dispatch({type: SETNAME, payload})
)

export const changeStatusOld = () => dispatch => (
    dispatch({type: STATUS_OLD})
)
export const changeStatusNew = () => dispatch => (
    dispatch({type: STATUS_NEW})
)
export const endGame = () => dispatch => {
    dispatch({type: END_GAME})
    }
export const startGame = () => dispatch => (
    dispatch({type: START_GAME})
)