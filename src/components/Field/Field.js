import React from 'react'
import {connect} from 'react-redux'


function createFielf(size) {
    const field =[]
    for(let i = 0; i < size; i++) {
        field[i] =<div className='field-item no-active' key={i+1}></div>
    }
    const block =[]
    for(let i = 0; i < size; i++) {
      block[i] = <div className='d-flex' key={i+2}>{field}</div>
    }

    return block
}

 function Field(props) {
    const{field} = props
    return(
        <div className='mt-4'>
            {createFielf(field)}
        </div>
    )
}

const mapStateToProps = state => ({
    field: state.field
})


export default connect(mapStateToProps)(Field)