import React, { useEffect, useState } from "react";
import axios from "axios";
import moment from "moment";
import { connect } from "react-redux";

import {
  setGameSettings,
  setPlayerName,
  changeStatusNew,
  changeStatusOld,
  startGame,
  endGame
} from "../../actions";
import Message from '../Message'
import randomm from "../../utils/randomBlock";


function GameControler(props) {
  const [settings, setSettings] = useState("");
  const [name, setName] = useState("");
  let [green, setGreen] = useState(0);
  let [red, setRed] = useState(0);
  let [winner, setWinner] = useState("");

  const {
    setGameSettings,
    newGame,
    setPlayerName,
    changeStatusNew,
    changeStatusOld,
    delay,
    gameStatus,
    startGame,
    endGame,
    field
  } = props;

  const boxCount = Math.pow(field, 2);
  useEffect(() => {
    axios
      .get("http://starnavi-frontend-test-task.herokuapp.com/game-settings")
      .then(res => setSettings(res.data));
  });

  const levels = Object.keys(settings);
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  function handleListener() {
    this.className = "field-item win";
    setGreen(green++);
  }

  async function gameStrarting(timer) {
     green = 0
      red = 0
    startGame();
    const div = document.querySelectorAll(".field-item");
    const arr = [...div];

    arr.map(item => {
      item.className = "field-item no-active";
    });
    takeRandom();

    function takeRandom() {
      randomm();
      const block = document.querySelector(".active");
      if (block) {
        block.addEventListener("click", handleListener);
      }
      setTimeout(() => {
        if (block.classList.contains("active")) {
          block.className = "field-item loose";
          block.removeEventListener("click", handleListener);
          setRed(red++);
        }
        if (red >= boxCount / 2 || green >= boxCount / 2) {
          red < green ? setWinner(name) : setWinner("Computer")
          endGame();
          return;
        }
        takeRandom();
      }, timer);
    }
  }

  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  function handleChangeSelect(e) {
    let data = e.target.value;
    if (data === "") {
      setGameSettings({ field: 0, delay: 0 });
      return;
    }
    setGameSettings({
      field: settings[data].field,
      delay: settings[data].delay
    });
  }

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        setPlayerName(name);
        changeStatusOld();
        gameStrarting(delay);
      }}
    >
      <div className="form-row align-items-center">
        <div className="col-auto">
          <select
            className="custom-select"
            required
            onChange={e => {
              handleChangeSelect(e);
            }}
          >
            <option value="">Choose level</option>
            {levels.map(item => (
              <option value={item} key={item}>
                {item}
              </option>
            ))}
          </select>
        </div>
        <div className="col-auto">
          <input
            type="text"
            className="form-control"
            placeholder="Enter your name"
            onChange={e => {
              setName(e.target.value);
              if (newGame) return;
              changeStatusNew();
            }}
            required
          />
        </div>
        <div className="col-auto">
          <button
            disabled={gameStatus === "start"}
            type="submit"
            className="btn btn-primary mb-2"
          >
            {newGame ? "Play" : "Play again"}
          </button>
        </div>
      </div>
      {gameStatus === "end" && (
        <Message winner={winner} date={moment(Date.now()).format("llll")}/>
      )}
    </form>
  );
}

const mapStateToPorops = state => ({
  newGame: state.newGame,
  field: state.field,
  delay: state.delay,
  gameStatus: state.gameStatus
});

export default connect(mapStateToPorops, {
  setGameSettings,
  startGame,
  endGame,
  setPlayerName,
  changeStatusNew,
  changeStatusOld
})(GameControler);
