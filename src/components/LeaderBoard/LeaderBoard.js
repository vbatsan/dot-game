import React, {useState, useEffect} from 'react'
import axios from 'axios'

export default function LeaderBoard() {
    const[table, setTable] = useState([])

    useEffect(() => {
        axios.get('https://starnavi-frontend-test-task.herokuapp.com/winners')
        .then(res => setTable(res.data))
    })

    return(
        <div className='leader-board'>
            <h2 className='text-center'>Leader Board</h2>
        <ul className='list-group'>
            {
                table.map(item => (
                    <li className='list-group-item list-group-item-dark d-flex justify-content-between align-items-center'
                    key={item.id}
                    >
                        <span>{item.winner}</span>
                        <span>{item.date}</span>
                    </li>
                ))
            }
        </ul>
        </div>
    )
}

