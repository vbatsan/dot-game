import React, {useEffect} from 'react'
import axios from 'axios'

export default function Message(props) {

    useEffect(() => {
        axios.post('https://starnavi-frontend-test-task.herokuapp.com/winners', {
            "winner": props.winner,
            "date": props.date
        })

    },[])

    return(
        <div>
    <h5 className='text-center'>{`${props.winner} won`}</h5>
    </div>
    )
}