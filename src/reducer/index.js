import{SETSETTINGS, SETNAME, STATUS_OLD, STATUS_NEW, END_GAME, START_GAME} from '../actions/types';

const initialState = {
    field: 0,
    delay:0,
    newGame: true,
    playerName: '',
    gameStatus:'ready'
}

export default function(state = initialState, action) {
    switch(action.type) {
        case SETSETTINGS:
            return{
                ...state,
                field: action.payload.field,
                delay: action.payload.delay
            }
        case SETNAME:
            return {
                ...state,
                playerName: action.payload
            }

        case STATUS_NEW:
            return{
                ...state,
                newGame: true
            }

        case STATUS_OLD:
            return{
                ...state,
                newGame:false
            }

        case END_GAME:
            return {
                ...state,
                gameStatus:'end'
            }

        case START_GAME:
            return {
                ...state,
                gameStatus: 'start'
            }
        default:
            return state
    }
}