import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducer';

const inititalState = {
        field: 0,
        delay:0,
        newGame: true,
        playerName: ''
};

const store = createStore(
    reducer,
    inititalState,
    compose(applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__() || compose));

export default store;